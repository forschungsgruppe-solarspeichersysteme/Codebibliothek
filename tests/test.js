const MathHelper = require('../component-library/helpers/mathHelper.js');

test('roundByStep(10.234,10) results in "10"', () => {
    expect(MathHelper.roundByStep(10.234,10)).toBe("10");
});