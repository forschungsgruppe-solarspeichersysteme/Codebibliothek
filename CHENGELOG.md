# Changelog

All notable changes to this project will be documented in this file. <br>
For each version, important additions, changes and removals are listed here.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The structure for this CHANGELOG can be seen on [rl-institut/super-repo](https://github.com/rl-institut/super-repo/blob/production/CHANGELOG.md)

## [current]

### Added

### Changed

### Removed

## [0.1.0] Initial Release - 2024-01-23

### Added
- Add initial setup [Merge Request](https://gitlab.com/forschungsgruppe-solarspeichersysteme/Codebibliothek/-/merge_requests/2)

### Changed

### Removed