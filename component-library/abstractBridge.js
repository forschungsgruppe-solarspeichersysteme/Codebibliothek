// The Bridge class acts as an intermediary between the Library and the tool itself
class AbstractBridge {
    // Declare instance variables for the Initializer and different handlers.
    initializer;
    preloadJsFiles = [];
    jsFiles = [];
    cssFiles = [];

    constructor(initializer) {
        if (new.target === AbstractBridge) {
            throw new TypeError("Cannot construct Bridge instances directly");
        }
        this.initializer = initializer;
    }

    // Initialize the Bridge
    init() {
        console.warn('You have to implement the method init!');
    }

    // In Initializer, method is called once everything is loaded
    finishedLoading(){
        console.warn('You have to implement the method finishedLoading!');
    }

    // In ViewHandler, method is called after switching sub views
    afterSwitchSubView(flowEscape) {
        console.warn('You have to implement the method afterSwitchSubView!');
    }

    // In ViewHandler, method is called after switching main views
    afterSwitchMainView(){
        console.warn('You have to implement the method afterSwitchMainView!');
    }

    // In ViewHandler, check for custom sub-view switch conditions
    checkForCustomSubViewSwitch(index, flowEscape) {
        console.warn('You have to implement the method checkForCustomSubViewSwitch!');
    }
    // In ViewHandler, check for custom sub-view switch conditions
    checkForCustomMainViewSwitch(index, flowEscape) {
        console.warn('You have to implement the method checkForCustomMainViewSwitch!');
    }
}
