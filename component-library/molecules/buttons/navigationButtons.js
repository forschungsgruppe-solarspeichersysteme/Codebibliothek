/**
 * Attributes: 
 * disableBack - (bool) disable back button
 * disableNext - (bool) disable next button
 * shakeBack - (bool) shaking back button
 * shakeNext - (bool) shaking next button
 * backValue (optional) - (string) innerText back Button, if no attr is set the text will be "Zurück"
 * nextValue (optional) - (string) innerText next Button, if no attr is set the text will be "Weiter"
 * backClickEvent - (string) click-event which is triggerd on back click
 * nextClickEvent - (string) click-event which is triggerd on next click
 * 
 * Attribute change callback:
 * disable-back
 * disable-next
 * shake-back
 * shake-next
 * next-value
 * back-value
 * back-click-event
 * next-click-event
 *
 * How to use:
 * <navigation-buttons disable-next="false" disable-back="false" back-value="Zurück" next-value="Weiter" next-click-event="sub-view-next" back-click-event="sub-view-back"></navigation-buttons>
 */

class NavigationButtons extends BaseElement {

    /**
     * DOM elements
     */
    backArrow
    nextArrow

    /**
     * Attributes
     */
    disableBack
    disableNext
    shakeBack
    shakeNext
    backValue
    nextValue
    backClickEvent
    nextClickEvent


    /**
     * Helper Variables
     */
    prevBackClickEvent
    prevNextClickEvent

    static get observedAttributes() {
        return ['disable-back', 'disable-next', 'shake-back', 'shake-next', 'next-value', 'back-value', 'next-click-event', 'back-click-event'];
    }

    static get observedAttributesFunction() {
        return {
            'disable-back' : function (newValue, that){
                that.disableBack = newValue == "true";
                that.updateElements()
            },
            'disable-next' : function (newValue, that){
                that.disableNext = newValue == "true";
                that.updateElements()
            },
            'shake-back' : function (newValue, that){
                that.shakeBack = newValue == "true";
                that.updateElements()
            },
            'shake-next' : function (newValue, that){
                that.shakeNext = newValue == "true";
                that.updateElements()
            },
            'back-value' : function (newValue, that){
                that.backValue = newValue;
                that.updateElements()
            },
            'next-value' : function (newValue, that){
                that.nextValue = newValue;
                that.updateElements()
            },
            'back-click-event' : function (newValue, that){
                that.backClickEvent = newValue;
            },
            'next-click-event' : function (newValue, that){
                that.nextClickEvent = newValue;
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.disableBack = this.getAttribute("disable-back") == "true"; 
        this.disableNext = this.getAttribute("disable-next") == "true";  
        this.shakeBack = this.getAttribute("shake-back") == "true"; 
        this.shakeNext = this.getAttribute("shake-next") == "true";  
        this.nextValue = this.getAttribute("next-value");  
        this.backValue = this.getAttribute("back-value");  
        this.nextValue = this.nextValue !== null ? this.nextValue : "Weiter"
        this.backValue = this.backValue !== null ? this.backValue : "Zurück"
        this.nextClickEvent = this.getAttribute("next-click-event");  
        this.backClickEvent = this.getAttribute("back-click-event");  
    }
    
    build(){

        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/buttons/navigationButtons.css")

        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        const backArrow = document.createElement("arrow-button");
        backArrow.className="back-arrow";
        backArrow.setAttribute("value", this.backValue)
        backArrow.setAttribute("shake", this.shakeBack)
        backArrow.setAttribute("back", "true")
        backArrow.setAttribute("disabled", this.disableBack)
        outerWrapper.appendChild(backArrow);

        const nextArrow = document.createElement("arrow-button");
        nextArrow.className="next-arrow";
        nextArrow.setAttribute("value", this.nextValue)
        nextArrow.setAttribute("shake", this.shakeNext)
        nextArrow.setAttribute("disabled", this.disableNext)
        outerWrapper.appendChild(nextArrow);
        
        
    }

    setEvents(){
        this.backArrow = this.shadowRoot.querySelector('.back-arrow');
        this.nextArrow = this.shadowRoot.querySelector('.next-arrow');
    }

    updateElements(){
        if(this.disableBack){
            this.backArrow.setAttribute("disabled", "true")
        }else{
            this.backArrow.setAttribute("disabled", "false")
        }
        if(this.disableNext){
            this.nextArrow.setAttribute("disabled", "true")
        }else{
            this.nextArrow.setAttribute("disabled", "false")
        }
        if(this.shakeBack){
            this.backArrow.setAttribute("shake", "true")
        }else{
            this.backArrow.setAttribute("shake", "false")
        }
        if(this.shakeNext){
            this.nextArrow.setAttribute("shake", "true")
        }else{
            this.nextArrow.setAttribute("shake", "false")
        }
        this.nextArrow.setAttribute("click-event", this.nextClickEvent);
        this.backArrow.setAttribute("click-event", this.backClickEvent);
        this.backArrow.setAttribute("value", this.backValue);
        this.nextArrow.setAttribute("value", this.nextValue);
    }
}
customElements.define('navigation-buttons', NavigationButtons)