/**
 * Attributes: 
 * buttons - (json-string) array of buttons to be displayed
 * value - (string) current value of the tab-panel
 * min-width (optional) - (string) min-width of button
 *
 * Attribute change callback:
 * disabled
 * value
 *
 * How to use:
 * <tab-button-panel buttons='["Energetisches", "Wirtschaftlich"]' value="1"></tab-button-panel>
 */

class TabButtonPanel extends BaseElement {

    /**
     * DOM elements
     */
    buttonElements

    /**
     * Attributes
     */
    buttons
    value
    minWidth


    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.buttons = JSON.parse(this.getAttribute("buttons"));
        this.minWidth = this.getAttribute("min-width");
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/buttons/tabButtonPanel.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        for (let i = 0; i < this.buttons.length; i++) {
            const button = document.createElement("tab-button");
            button.setAttribute("value", this.buttons[i]);
            button.setAttribute("inactive", "false");

            if(this.minWidth != null){
                button.setAttribute("min-width", this.minWidth);
            }
            this.template.content.appendChild(button);
        }
        
    }

    setEvents(){
        this.buttonElements = this.shadowRoot.querySelectorAll('tab-button');
        for (let i = 0; i < this.buttonElements.length; i++) {
            this.buttonElements[i].addEventListener('click', () => {
                this.setAttribute("value", i);
            });
        }
        this.updateElements();
    }

    updateElements(){
        for (let i = 0; i < this.buttonElements.length; i++) {
            if(i == this.value){
                this.buttonElements[i].setAttribute("inactive", "false");
            }else{
                this.buttonElements[i].setAttribute("inactive", "true");
            }
        }
    }
}
customElements.define('tab-button-panel', TabButtonPanel)
