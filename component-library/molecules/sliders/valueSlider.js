/**
 * Attributes: 
 * min - (int) Minimal slider value
 * max - (int) Maximum slider value
 * step - (int) discrete step distance between min and max (pay attention that step makes sense)
 * value - (int) current value on the slider
 * label (optional) - (string) shows a label above the slider
 * unit - (string) unit to show right of the input field
 * invisible-label - (string) invisible label for people who can't see
 * tick-step (optional) - (int) discrete tick-step distance between min and max
 * number-nth-tick (optional) - (int) number on every n-th tick on the slider
 * 
 * Attribute change callback:
 * value
 * 
 * How to use:
 * <value-slider min=0 max=1000 step=100 value=300 label="Label" unit="kwH" invisible-label="Umschaltfläche Wärmepumpe"></value-slider>
 */

class ValueSlider extends BaseElement {
    /**
     * DOM elements
     */
    input
    slider

     /**
     * Attributes
     */
    label
    unit
    min
    max
    step
    value
    invisibleLabel

    tempValue

    static get observedAttributes() {
        return ['value'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.unit = this.getAttribute("unit")
        this.min = parseInt(this.getAttribute("min"))
        this.max = parseInt(this.getAttribute("max"))
        this.step = parseFloat(this.getAttribute("step"))
        this.value = parseInt(this.getAttribute("value"))
        this.invisibleLabel = this.getAttribute("invisible-label")
        this.tickStep = parseInt(this.getAttribute("tick-step"))
        this.numberNthTick = parseInt(this.getAttribute("number-nth-tick"))
    }

    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("molecules/sliders/valueSlider.css")

         //Add HTML elements
         if(this.label !== null){
             const labelSpan = document.createElement("span");
             labelSpan.className="label";
             labelSpan.innerText=this.label;
             this.template.content.appendChild(labelSpan)
         }

         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

         const rangeSlider = document.createElement("range-slider");
         rangeSlider.className="range-slider";
         rangeSlider.setAttribute("value", this.value);
         rangeSlider.setAttribute("step", this.step);
         rangeSlider.setAttribute("min", this.min);
         rangeSlider.setAttribute("max", this.max);
         rangeSlider.setAttribute("drag", false);
         rangeSlider.setAttribute("tick-step", this.tickStep);
         rangeSlider.setAttribute("number-nth-tick", this.numberNthTick);
         outerWrapper.appendChild(rangeSlider)

         const inputField = document.createElement("number-input");
         inputField.setAttribute("invisible-label", this.invisibleLabel);
         inputField.setAttribute("value", this.value);
         inputField.setAttribute("step", this.step);
         inputField.setAttribute("min", this.min);
         inputField.setAttribute("max", this.max);
         inputField.setAttribute("unit", this.unit);
         outerWrapper.appendChild(inputField)        
 
        //  const unitLabel = document.createElement("span");
        //  unitLabel.className="unit-label";
        //  unitLabel.innerText=this.unit;
        //  outerWrapper.appendChild(unitLabel)        

         this.shadowRoot.appendChild(this.template.content.cloneNode(true));
    }

    setEvents(){
           //adding element references 
           this.input = this.shadowRoot.querySelector('number-input');
           this.slider = this.shadowRoot.querySelector('range-slider');
           let that = this
   
           //observe elements for value changes
           var observer = new MutationObserver(function(mutations) {
               mutations.forEach(function(mutation) {
   
                   if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.tempValue) {
                       //equual to mutation.target instanceof NumberInputElement
                       that.tempValue = mutation.target.getAttribute("value")
                        if(that.slider.getAttribute("drag") == "false"){
                            that.setAttribute("value", mutation.target.getAttribute("value"));
                        }
                       switch(mutation.target.constructor){
                           case NumberInput:
                                that.slider.setAttribute("value", mutation.target.getAttribute("value"));
                                break;
                           case RangeSlider:
                                that.input.setAttribute("value", mutation.target.getAttribute("value"));
                                break;
                           }
                   }
                   if (mutation.type === "attributes" && mutation.attributeName === "drag" && mutation.target.getAttribute("drag") === "false") {
                        that.setAttribute("value", that.tempValue);
                    }
               });
           });
       
           [this.input,this.slider].forEach((node) => {
               observer.observe(node, {
                   attributes: true
               });
           })
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            this.value = newValue;
            this.tempValue = newValue;
            this.input.setAttribute("value", newValue);
            this.slider.setAttribute("value", newValue);
        }
    }
}
customElements.define('value-slider', ValueSlider)
