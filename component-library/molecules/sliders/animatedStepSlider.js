/**
 * Attributes: 
 * options - (json-string) array of options to be displayed 
 * value - (int) decides which option is selected (0 = first element of options sarray)
 * animation - (string) animation name to show
 * additional-animation-value - Additional value for animation
 * label (optional) - (string) shows a label above the slider
 * unit - (string) unit to show right of the input field

 * Attribute change callback:
 * value
 * 
 * How to use:
 * <animated-step-slider options='["15","30","45"]' value="1" label="Um wie viel Grad ist Ihre PV-Anlage geneigt?" unit="°" animation="tiltable-solar-roof"></animated-step-slider>
 */

class AnimatedStepSlider extends BaseElement {
    /**
     * DOM elements
     */
    slider
    valueSpan
    animationElement

    /**
     * Attributes
     */
    options
    value
    animation
    additionalAnimationValue
    unit
    label

    static get observedAttributes() {
        return ['value', 'additional-animation-value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.slider.setAttribute("value", that.value)
                that.updateElements()
            },
            'additional-animation-value' : function (newValue, that){
                that.additionalAnimationValue = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.options = JSON.parse(this.getAttribute("options"));
        this.value = this.getAttribute("value");
        this.animation = this.getAttribute("animation");
        this.unit = this.getAttribute("unit");
        this.label = this.getAttribute("label")
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("molecules/sliders/animatedStepSlider.css")

        //Add HTML elements
        if(this.label !== null){
            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            this.template.content.appendChild(labelSpan)
        }

         
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

         const animationElement = document.createElement(this.animation);
         animationElement.className="animation";
         animationElement.setAttribute("value", this.options[this.value])
         animationElement.setAttribute("additional-animation-value", this.additionalAnimationValue)
         outerWrapper.appendChild(animationElement);

         const stepSlider = document.createElement("step-slider");
         stepSlider.setAttribute("value", this.value);
         stepSlider.setAttribute("options", JSON.stringify(this.options));
         stepSlider.setAttribute("unit", this.unit);

         outerWrapper.appendChild(stepSlider);

         const valueSpan = document.createElement("span");
         valueSpan.className = "value-span";
         valueSpan.innerText = this.options[this.value] + this.unit;
         outerWrapper.appendChild(valueSpan);
    }

    setEvents(){
        this.slider = this.shadowRoot.querySelector('step-slider');
        this.valueSpan = this.shadowRoot.querySelector('.value-span');
        this.animationElement = this.shadowRoot.querySelector('.animation');

        let that = this;
        //observe elements for value changes
        var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
                if (mutation.type === "attributes" && mutation.attributeName === "value" && mutation.target.getAttribute("value") !== that.value) {
                    that.setAttribute("value", mutation.target.getAttribute("value"))
                }
            });
        });
    
        [this.slider].forEach((node) => {
            observer.observe(this.slider, {
                attributes: true
            });
        })
    }

    updateElements(){
        this.valueSpan.innerText = this.options[this.value] + this.unit;
        this.animationElement.setAttribute("value", this.options[this.value])
        this.animationElement.setAttribute("additional-animation-value", this.additionalAnimationValue);
    }

    setValue(value){    
        this.inputs.forEach(input => {
            input.setAttribute("checked", this.value == input.getAttribute("index"));
        });

        if(this.getAttribute("value") != value){
            this.setAttribute("value", value)
        }
    }
}
customElements.define('animated-step-slider', AnimatedStepSlider)
