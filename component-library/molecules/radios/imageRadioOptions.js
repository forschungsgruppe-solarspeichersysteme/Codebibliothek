/**
 * Attributes: 
 * options - (json-string) array of options to be displayed 
 * value - (int) decides which option is selected (0 = first element of options sarray)
 * src - (json-string) array of src url's for the images corresponding to the options
 * darkSrc - (json-string) array of src url's for the dark images corresponding to the options
 * alt - (json-string) array of alt texts for the images
 * label- (string) shows a label above the radio options
 * img-width - (string) width of the images in em, % or other units which are supported by css
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <image-radio-options id="ankunftszeit" options='["nachmittags","abends"]' alt='["Auto nachmittags ladend","Auto abends ladend"]' src='["img/charging-day.svg","img/charging-night.svg"]' dark-src='["img/charging-day-dark.svg","img/charging-night-dark.svg"]' img-width="7em" value="0" label="Wann schließen Sie Ihr Elektroauto gewöhnlich zum Laden an?"></image-radio-options>
 */

class ImageRadioOptions extends BaseElement {
    /**
     * DOM elements
     */
    radioOptions
    images

    /**
     * Attributes
     */
    options
    value
    src
    darkSrc
    label
    imgWidth
    alt

    static get observedAttributes() {
        return ['value'];
    }
    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.options = JSON.parse(this.getAttribute("options"));
        this.value = this.getAttribute("value");
        this.src = JSON.parse(this.getAttribute("src"));
        this.darkSrc = JSON.parse(this.getAttribute("dark-src"));
        this.alt = JSON.parse(this.getAttribute("alt"));
        this.label = this.getAttribute("label");
        this.imgWidth = this.getAttribute("img-width")
    }
    
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("molecules/radios/imageRadioOptions.css")

        //Add HTML elements
        if(this.label !== null){
            const labelSpan = document.createElement("span");
            labelSpan.className="label";
            labelSpan.innerText=this.label;
            this.template.content.appendChild(labelSpan)
        }
       
         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);
  
         for (let i = 0; i < this.options.length; i++) {
            const pic = document.createElement("picture");
            pic.style.width = this.imgWidth
            const darkSource = document.createElement("source");
            darkSource.media = "(prefers-color-scheme: dark)"
            darkSource.srcset = this.darkSrc[i]
            darkSource.alt = this.alt[i]
            pic.appendChild(darkSource);

            const img = document.createElement("img");
            img.src= this.src[i];
            img.alt= this.alt[i];
            img.setAttribute("index", i);
            pic.appendChild(img);
            outerWrapper.appendChild(pic);

         }

         const radioOptions = document.createElement("radio-options");
         radioOptions.setAttribute("options", JSON.stringify(this.options))
         radioOptions.setAttribute("horizontal", "true")
         radioOptions.setAttribute("value", this.value)
         this.template.content.appendChild(radioOptions);
 
         
    }

    setEvents(){
        this.radioOptions = this.shadowRoot.querySelector('radio-options');
        this.images = this.shadowRoot.querySelectorAll('picture');

        this.createObserver(this.radioOptions, "value");

        for(let i = 0; i < this.images.length; i++) {
            this.images[i].onclick = (e) => {
                this.setAttribute("value", e.target.getAttribute("index"))
            };
        }
        this.updateElements();
    }

    updateElements(){
        if(this.radioOptions.getAttribute("value") != this.value){
            this.radioOptions.setAttribute("value", this.value)
        }
        
        for(let i = 0; i < this.images.length; i++) {
            if(i == this.value){
                this.images[i].classList.add("active")
            }else{
                this.images[i].classList.remove("active")
            }  
        }
    }
}
customElements.define('image-radio-options', ImageRadioOptions)
