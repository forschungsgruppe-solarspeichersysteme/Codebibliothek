/**
 * Attributes: 
 * value - (string) innerText of Button
 * unit - (string) unit which is shown in bold after the value
 * text - (string) text which is shown under the value and unit
 * animation-atom - (string) animation atom to bind
 * src - (string) src-path of the image
 * alt - (string) alt text for image
 * mask-src - (string) src-path of the mask-image


 * text-left (optional) - (bool) set to true if the text should be shown left and the image right
 * fill - (float) precentage of the infill of the animation
 * minimalistic - (bool) if true, a minimalistic version is shown
 * 
 * Attribute change callback:
 * value
 * fill
 * minimalistic
 * text-left
 *
 * How to use:
 * <result-panel animation-atom="fill-result-animation" alt="Sparschwein" src="img/results/piggybank.svg" mask-src="img/results/inner-piggybank.svg" fill="50" text="" unit="€" value="750" text-left="false"></result-panel>
 */

class ResultPanel extends BaseElement {

    /**
     * DOM elements
     */
    valueElement
    outerWrapper
    resultAnimation
    labelSpan

    /**
     * Attributes
     */
    value
    unit
    text 
    textLeft
    animationAtom
    src
    maskSrc
    alt
    fill
    minimalistic

    static get observedAttributes() {
        return ['value', 'fill', 'minimalistic', 'text-left', 'text'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            },
            'fill' : function (newValue, that){
                that.fill = Math.round(newValue);
                that.updateElements()
            },
            'minimalistic' : function (newValue, that){
                that.minimalistic = newValue;
                that.updateElements()
            },
            'text-left' : function (newValue, that){
                that.textLeft = newValue == "true"
                that.updateElements()
            },
            'text' : function (newValue, that){
                that.text = newValue;
                that.updateElements()
            },
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.unit = this.getAttribute("unit");
        this.value = this.getAttribute("value");
        this.text = this.getAttribute("text");
        this.fill = this.getAttribute("fill");
        this.minimalistic = this.getAttribute("minimalistic");
        this.animationAtom = this.getAttribute("animation-atom");
        this.src = this.getAttribute("src");
        this.alt = this.getAttribute("alt");
        this.maskSrc = this.getAttribute("mask-src");
        this.textLeft = this.getAttribute("text-left") == "true";

    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("molecules/panels/resultPanel.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        let animation = document.createElement(this.animationAtom);
        animation.className="animation"
        animation.setAttribute("src", this.src)
        animation.setAttribute("alt", this.alt)
        animation.setAttribute("mask-src", this.maskSrc)
        animation.setAttribute("fill", this.fill)

        // img.src = this.src;
        outerWrapper.appendChild(animation);

        const innerWrapper = document.createElement("div");
        innerWrapper.className="inner-wrapper";
        outerWrapper.appendChild(innerWrapper);

        const textWrapper = document.createElement("div");
        textWrapper.className="text-wrapper";
        innerWrapper.appendChild(textWrapper);

        const value = document.createElement("span");
        value.className="value";
        textWrapper.appendChild(value)

        const labelSpan = document.createElement("span");
        labelSpan.className="text";
        // labelSpan.innerText=this.text;
        textWrapper.appendChild(labelSpan)

        
    }

    setEvents(){
        this.valueElement = this.shadowRoot.querySelector('.value');
        this.resultAnimation = this.shadowRoot.querySelector('.animation');
        this.outerWrapper = this.shadowRoot.querySelector('.outer-wrapper');
        this.labelSpan = this.shadowRoot.querySelector('.text');

        this.updateElements()
    }

    updateElements(){
        this.valueElement.innerText = this.value + " " + this.unit
        this.resultAnimation.setAttribute("fill", this.fill)
        this.outerWrapper.classList.toggle("minimal", this.minimalistic === "true")

        this.labelSpan.innerText = this.text;

        if(this.textLeft){
            this.outerWrapper.classList.add("reverse")
        }else{
            this.outerWrapper.classList.remove("reverse")
        }
    }
}
customElements.define('result-panel', ResultPanel)
