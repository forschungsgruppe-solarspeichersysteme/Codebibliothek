/**
 * Attributes: 
 * fill - (float) precentage of the rotary infill of the animation
 * src - (string) src-path of the image
 * alt - (string) alt text for image
 *
 * Attribute change callback:
 * fill
 *
 * How to use:
 * <circle-animation fill="50" animation="outlet"></circle-animation>
 */

class CircleResultAnimation extends BaseElement {
    /**
     * DOM elements
     */
    fillCircle;
 
    /**
     * Attributes
     */
    fill;
    src;
    alt;

    static get observedAttributes() {
        return ['fill'];
    }

    static get observedAttributesFunction() {
        return {
            'fill' : function (newValue, that){
                that.fill = newValue;
                that.updateElements();
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.fill = this.getAttribute("fill");
        this.src = this.getAttribute("src");
        this.alt = this.getAttribute("alt");
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/animations/circleResultAnimation.css")
    
        //HTML elements structure (Indentation stands for the nested structure)
        const outerWrapper = document.createElement("div");
        outerWrapper.className="outer-wrapper";
        this.template.content.appendChild(outerWrapper);

        const animation = document.createElementNS("http://www.w3.org/2000/svg","svg");
        animation.setAttribute("class", "progress-ring");
        outerWrapper.appendChild(animation);

        const circle1 = document.createElementNS("http://www.w3.org/2000/svg","circle");
        //  circle.className="progress-ring_circle";
        circle1.setAttribute("stroke", "#d0d0d0");
        circle1.setAttribute("stroke-width", ".5em");
        circle1.setAttribute("fill", "transparent");
        circle1.setAttribute("r", "1.75em");
        circle1.setAttribute("cx", "2em");
        circle1.setAttribute("cy", "2em");
        animation.appendChild(circle1);
        const circle = document.createElementNS("http://www.w3.org/2000/svg","circle");
        //  circle.className="progress-ring_circle";
        circle.setAttribute("class", "progress-ring_circle");
        circle.setAttribute("stroke", "#76b921");
        circle.setAttribute("stroke-width", ".5em");
        circle.setAttribute("fill", "transparent");
        circle.setAttribute("r", "1.75em");
        circle.setAttribute("cx", "2em");
        circle.setAttribute("cy", "2em");
        animation.appendChild(circle);

        const svg = document.createElement("img");
        svg.src=this.src;
        svg.alt=this.alt;
        outerWrapper.appendChild(svg);
       
    }

    setEvents(){
        this.fillCircle = this.shadowRoot.querySelector('circle:last-of-type');
        var radius = this.fillCircle.r.baseVal.value;
        var circumference = radius * 2 * Math.PI;
        this.fillCircle.style.strokeDasharray = `${circumference} ${circumference}`;
        this.fillCircle.style.strokeDashoffset = `${circumference}`;
        this.updateElements();

    }

    updateElements(){
        var radius = this.fillCircle.r.baseVal.value;
        var circumference = radius * 2 * Math.PI;
        const offset = circumference - this.fill / 100 * circumference;
        this.fillCircle.style.strokeDasharray = `${circumference} ${circumference}`;
        this.fillCircle.style.strokeDashoffset = offset;
    }

}
customElements.define('circle-result-animation', CircleResultAnimation);
