/**
 * Attributes: 
 * options - (json-string) array of options to be displayed 
 * unit - (string) unit to show right of the input field
 * value - (int) current value on the slider
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <step-slider options='["15","30","45"]' value="30" unit="°"></step-slider>
 */

class StepSlider extends BaseElement {
    /**
     * DOM elements
     */
    sliderStops
    thumb
    bar

    /**
     * Attributes
     */
    value
    options
    unit

    bMouseDownThumb = false;

    static get observedAttributes() {
        return ['value'];
    }

    static get observedAttributesFunction() {
        return {
            'value' : function (newValue, that){
                that.value = newValue;
                if(!that.bMouseDownThumb){
                    that.moveElementsOnValue()
                }
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.options = JSON.parse(this.getAttribute("options"));
        this.unit = this.getAttribute("unit");
    }
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/sliders/stepSlider.css")

         //HTML elements structure (Indentation stands for the nested structure)
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         outerWrapper.setAttribute('draggable', false);
         this.template.content.appendChild(outerWrapper);
 
            const sliderBox = document.createElement("div");
            sliderBox.className="slider-box";
            sliderBox.setAttribute('draggable', false);
            sliderBox.setAttribute('role', "radiogroup");
            outerWrapper.appendChild(sliderBox);

                for (let i = 0; i < this.options.length; i++) {
                    const sliderStop = document.createElement("div");
                    sliderStop.className="slider-stop";
                    sliderStop.setAttribute('draggable', false);
                    sliderStop.setAttribute('role', 'radio');
                    sliderStop.setAttribute('aria-label', this.options[i]+this.unit);
                    if(i == this.value){
                        sliderStop.setAttribute("aria-checked", "true")
                        sliderStop.tabIndex="0";
                    }else{
                        sliderStop.setAttribute("aria-checked", "false")
                        sliderStop.tabIndex="-1";
                    }
                    sliderStop.style.width = 100 / this.options.length + "%";
                    sliderStop.setAttribute("index", i); 
                    sliderBox.appendChild(sliderStop);
                }

                const sliderThumb = document.createElement("div");
                sliderThumb.className="slider-thumb";
                sliderThumb.setAttribute('draggable', false);
                sliderThumb.style.width = 100 / this.options.length + "%";
                sliderBox.appendChild(sliderThumb);

    }

    moveElementsOnValue(){
        this.thumb.style.left = (100 / this.options.length * this.value) + this.getPercentPerStep() / 2 + "%";
    }

    setValue(target){
        if(this.getAttribute("value") != target.getAttribute("index")){
            for(let j = 0; j < this.sliderStops.length; j++) {
                this.sliderStops[j].setAttribute("aria-checked", "false");
                this.sliderStops[j].tabIndex="-1";
            }
            target.tabIndex="0";
            target.setAttribute("aria-checked", "true")
            this.setAttribute("value", target.getAttribute("index"))
        }
    }

    setEvents(){
        //adding element references
        let that = this;
        this.sliderBox = this.shadowRoot.querySelector('.slider-box');
        this.sliderStops = this.shadowRoot.querySelectorAll('.slider-stop');
        this.thumb = this.shadowRoot.querySelector('.slider-thumb');
        this.bar = this.shadowRoot.querySelector('.slider-box');

        for(let i = 0; i < this.sliderStops.length; i++) {
            this.sliderStops[i].onclick = (e) => {
               that.setValue(e.target);
            };
        }
       
        this.sliderBox.addEventListener('keydown', function(e) {
            let newValue = null;

            //on arrow up/left
            if(e.which==37 || e.which==38){
                newValue = that.value == 0 ? that.options.length-1 : that.value-1 
            }

            //on arrow down/right
            if(e.which==39 || e.which==40){
                newValue = that.value == that.options.length-1 ? 0 : parseInt(that.value)+1 
            }

            if(newValue !== null){
                e.preventDefault()
                that.setAttribute("value", newValue)
                for(let j = 0; j < that.sliderStops.length; j++) {
                    that.sliderStops[j].tabIndex="-1";
                    if(j==newValue){
                        that.sliderStops[j].tabIndex="0";
                        that.sliderStops[j].focus();
                    }

                }
            }
        });

         //Event listeners for mouse drag
        this.thumb.addEventListener('mousedown', (e) => {
            this.setMouseDownThumb(true)
            this.thumb.setPointerCapture(1);
        });

        this.thumb.addEventListener('mouseup', (e) => {
            this.setMouseDownThumb(false)
            this.thumb.releasePointerCapture(1);
        });

        this.thumb.addEventListener('touchstart', (e) => {
            e.preventDefault(); 
            this.setMouseDownThumb(true)
            // this.thumb.setPointerCapture(e.pointerId);
        });
        
        this.thumb.addEventListener('touchend', (e) => {
            e.preventDefault(); 
            this.setMouseDownThumb(false)
            // this.thumb.releasePointerCapture(e.pointerId);
        });

        window.addEventListener("mousemove",function(e){
            // console.log("mouemove")
            if(that.bMouseDownThumb){
                window.getSelection().removeAllRanges();
                var bounds = e.target.getBoundingClientRect();
                var x = e.clientX - bounds.left;
                that.moveElements(x);
            }
           
        });

         window.addEventListener("touchmove",function(e){

            if(that.bMouseDownThumb){
                window.getSelection().removeAllRanges();

                var bounds = e.target.getBoundingClientRect();
                var x = e.touches[0].clientX - bounds.left;
                that.moveElements(x);
            }
        });

        this.bar.onpointerdown = (e) => {

            if(e.target !== this.thumb){
                var bounds = e.target.getBoundingClientRect();
                var x = e.clientX - bounds.left;
                that.moveElements(x);
                this.setMouseDownThumb(true)
                this.bar.setPointerCapture(e.pointerId);
            }
            
        };
        this.bar.onpointerup  = (e) => {
            this.setMouseDownThumb(false)
            this.bar.releasePointerCapture(e.pointerId);
        };

        this.bar.addEventListener('touchend', (e) => {
            // console.log("bar touchend")
            e.preventDefault(); 
            this.setMouseDownThumb(false)
        });

        this.moveElementsOnValue();
    }

    setMouseDownThumb(value){
        if(this.bMouseDownThumb && !value){
            this.moveElementsOnValue();
        }
        this.bMouseDownThumb = value;
    }

    /**
     * Move slider element because of user interaction
     * 
     * @param {Float} clientX 
     */
      moveElements(offset, bPercent = false){
        if(!bPercent){
            if(this.bar.clientWidth == 0)
                return;
            offset = (offset / this.bar.clientWidth) * 100;
        }
        // offset += this.getPercentPerStep()/2;


        // -1 is set because if the width is a float the browser rounds the value to the nearest int in js, but in the browser it is still a float. 
        // This results in a possible infill longer tzhan the bar itself. 

        let leftPercentageOffset = Math.max(Math.min(offset, 100 - this.getPercentPerStep()/2), 0 + this.getPercentPerStep()/2);
        this.thumb.style.left = leftPercentageOffset + "%";
        this.updateValue(leftPercentageOffset)
    }

     /**
     * Checks if the the value needs to change based on user interaction. If so it sets the value attribute
     * 
     * @param {FLoat} leftPixelOffset 
     */
     updateValue(leftPercentageOffset){
        let currentStep = Math.floor(leftPercentageOffset / this.getPercentPerStep());
        let newValue = currentStep;

        //round it
        newValue = Math.round(newValue)

        if(this.value != newValue){
            this.value = newValue;
            this.setAttribute("value", newValue)
        }
    }

     /**
     * Calculates the amount of pixels that the bar can move before the value increses or decreses. 
     * 
     * @returns {Float}
     */
     getPercentPerStep(){
        return 100 / this.options.length;
    }
}
customElements.define('step-slider', StepSlider)
