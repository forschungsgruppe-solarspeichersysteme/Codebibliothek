/**
 * Attributes: 
 * value - (bool) current value of the switch
 * label - (string) label for input
 * aria-label (optional) - (string) invisible label for people who can't see
 * 
 * Attribute change callback:
 * value
 *
 * How to use:
 * <toggle-switch value="true" aria-label=""></toggle-switch>
 */

class ToggleSwitch extends BaseElement {
    /**
     * DOM elements
     */
    input
    wrapper

    /**
     * Attributes
     */
    label
    value
    ariaLabel

    static get observedAttributes() {
        return ['value'];
    }

    setAttributes(){
        //Attributes saved as globals
        this.label = this.getAttribute("label")
        this.value = this.getAttribute("value")
        this.ariaLabel = this.getAttribute("aria-label")
    }
    build(){
         //Add Stylesheet to template
         this.addOwnStylesheet("atoms/toggles/toggleSwitch.css")
       
         //HTML elements structure (Indentation stands for the nested structure)
 
         const outerWrapper = document.createElement("div");
         outerWrapper.className="outer-wrapper";
         this.template.content.appendChild(outerWrapper);

        const innerWrapper = document.createElement("div");
        innerWrapper.className="inner-wrapper";
        outerWrapper.appendChild(innerWrapper);

        const inputWrapper = document.createElement("div");
        inputWrapper.className="switch";
        inputWrapper.tabIndex="0";
        inputWrapper.role="switch";
        inputWrapper.setAttribute("aria-checked", this.value);
        innerWrapper.appendChild(inputWrapper);

        const input = document.createElement("input");
        input.name="1"
        input.id="1"
        input.type = "checkbox"
        input.tabIndex="-1"
        input.checked = this.value == "true";
        inputWrapper.appendChild(input);

        const span = document.createElement("span");
        span.className="slider";
        span.setAttribute("aria-hidden","true")
        inputWrapper.appendChild(span);

        const arrow = document.createElementNS("http://www.w3.org/2000/svg","svg");
        arrow.setAttribute("fill", "#3b5d10");
        arrow.setAttribute("width", "1.2em");
        arrow.setAttribute("height", "1.2em");
        arrow.setAttribute("viewBox", "0 0 24 24");
        span.appendChild(arrow);

        const polygon = document.createElementNS("http://www.w3.org/2000/svg","polygon");
        polygon.setAttribute("points", "9.707 14.293 19 5 20.414 6.414 9.707 17.121 4 11.414 5.414 10");
        polygon.setAttribute("fill-rule", "evenodd");

        arrow.appendChild(polygon);

        const label = document.createElement("label");
        label.setAttribute("for", "1") 
        if(this.ariaLabel !== null){
            label.setAttribute("aria-label", this.ariaLabel) 
        }
        label.innerText = this.label;
        innerWrapper.appendChild(label)

    }

    setEvents(){
        let that = this;
        //adding element references 
        this.input = this.shadowRoot.querySelector('input');
        this.wrapper = this.shadowRoot.querySelector('.switch');

        //Event listeners for checkbox press
        this.input.oninput = (e) => {
            this.input.checked = !this.input.checked
            this.setAttribute("value", this.input.checked)
        };
        this.wrapper.onclick = (e) => {
            this.input.checked = !this.input.checked
            this.setAttribute("value", this.input.checked)
        };

        this.wrapper.addEventListener('keydown', function(e) {
            if(e.which==13 || e.which==32){
                e.preventDefault()
                that.wrapper.click()
            }
        });
    }

    attributeChangedCallback(propertyName, oldValue, newValue){
        //update value if changed
        if(propertyName === "value" && oldValue !== null){
            newValue = (newValue === 'true');
            this.value = newValue;
            console.log(newValue);
            this.input.checked = newValue;
            this.wrapper.setAttribute("aria-checked", newValue);
        }
    }
}
customElements.define('toggle-switch', ToggleSwitch)
