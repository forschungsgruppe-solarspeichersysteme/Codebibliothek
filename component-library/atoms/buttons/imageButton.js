/**
 * Attributes: 
 * src - (string) src-path of the image
 * alt - (string) alt text for image
 * width - (string) width of the image
 * 
 * Attribute change callback:
 * None
 *
 * How to use:
 * <image-button src="img/HTW_small.svg" alt="HTW Berlin Logo" width="6em"></image-button>
 */

class ImageButton extends BaseElement {

    /**
     * DOM elements
     */
    img

    /**
     * Attributes
     */
    src
    alt
    width

    setAttributes(){
        //Attributes saved as globals
        this.src = this.getAttribute("src");
        this.alt = this.getAttribute("alt");
        this.width = this.getAttribute("width");
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/buttons/imageButton.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const img = document.createElement("img");
        img.src = this.src;
        img.alt = this.alt;
        img.style.width = this.width;
        this.template.content.appendChild(img);
    }
}
customElements.define('image-button', ImageButton)
