/**
 * Attributes: 
 * value - (string) innerText of Button
 * type (optional) - (string) which type of button to display (grey, link, tab)
 * disabled - (bool) is button disabled or not
 * 
 * Attribute change callback:
 * disabled
 * value
 *
 * How to use:
 * <htw-button value="6" disabled type="link"></htw-button>
 */

class HTWButton extends BaseElement {

    /**
     * DOM elements
     */
    button

    /**
     * Attributes
     */
    value
    type
    disabled

    static get observedAttributes() {
        return ['disabled', 'value'];
    }

    static get observedAttributesFunction() {
        return {
            'disabled' : function (newValue, that){
                that.disabled = newValue == "true";
                that.updateElements()
            },
            'value' : function (newValue, that){
                that.value = newValue;
                that.updateElements()
            }
        };
    }

    setAttributes(){
        //Attributes saved as globals
        this.value = this.getAttribute("value");
        this.type = this.getAttribute("type");
        this.disabled = this.getAttribute("disabled") == "true";
    }
    
    build(){
        //Add Stylesheet to template
        this.addOwnStylesheet("atoms/buttons/htwButton.css")
            
        //HTML elements structure (Indentation stands for the nested structure)
        const button = document.createElement("button");
        if(this.type != null){
            button.classList.add(this.type)
        }
        this.template.content.appendChild(button);

        
    }

    setEvents(){
        this.button = this.shadowRoot.querySelector('button');
        this.updateElements();
    }

    updateElements(){
        if(this.disabled){
            this.button.setAttribute("disabled", "")
        }else{
            this.button.removeAttribute("disabled")
        }
        this.button.innerText = this.value;
    }
}
customElements.define('htw-button', HTWButton)
