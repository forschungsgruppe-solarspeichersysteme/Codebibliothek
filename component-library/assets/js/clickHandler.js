class ClickHandler {
     // Declare instance variables.
     initializer;

    // Constructor to set up the ViewHandler.
    constructor(initializer) {
        this.initializer = initializer;
        this.initClickListeners();
    }

    initClickListeners(){
        const buttons = SelectorHelper.querySelectorAll(document, ["[click-event]"]);

        // Iterate through buttons and set up event listeners for switching views.
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].addEventListener("click", (e) => {
                e.preventDefault();
                let flowEscape = e.currentTarget.getAttribute("flow-escape") == "true";
                let clickEvents = e.currentTarget.getAttribute("click-event").split(';');
                clickEvents.forEach(element => {
                    let selector = element.split(':')[0];
                    let value = element.split(':')[1];
                    switch(selector){
                        case "main-view":
                            this.initializer.viewHandler.switchMainViewById(value, flowEscape);
                            break;   
                        case "sub-view":
                            this.initializer.viewHandler.switchSubViewById(value, flowEscape);
                            break;    
                        case "custom-event":
                            this.initializer.bridge.handleCustomEvent(value);
                            break;
                    }                    
                });
            });
        }
    }
}